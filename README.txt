CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

This module provides "Youtube Plugin" plugin for CKEditor installed
with Wysiwyg.

For a full description of the project visit the project page:
https://www.drupal.org/project/wysiwyg_youtube

To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/wysiwyg_youtube


REQUIREMENTS
------------

This module requires the following module:

 * Wysiwyg (https://www.drupal.org/project/wysiwyg)


INSTALLATION
------------

 * Install as usual. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Download "Youtube Plugin":
   https://ckeditor.com/cke4/addon/youtube,

   unzip it and place "youtube" directory into
   "sites/all/libraries/ckeditor/plugins" directory. As a result there will
   be "plugin.js" file in "sites/all/libraries/ckeditor/plugins/youtube"
   directory.


CONFIGURATION
-------------

 * Go to "Wysiwyg profiles" page: /admin/config/content/wysiwyg.

 * Edit a Wysiwyg profile and enable "YouTube" button.
